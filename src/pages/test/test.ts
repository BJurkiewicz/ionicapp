import { Component, NgZone } from '@angular/core';
import { NavController } from 'ionic-angular'

import { DBMeter } from '@ionic-native/db-meter';
import * as moment from 'moment';
import * as _ from 'lodash'

export class Entry {
    counter: number;
    tap: number;
    diff: number;
    class: string;
}

const DATA: Entry[] = []

@Component({
    selector: 'page-test',
    templateUrl: 'test.html'
})

export class TestPage {

    decibelArr: Array<number> = [];
    decibels: any = 0;
    counter: number = 10;
    counterArr: Array<number> = [];
    counterInterval: any;
    tapArr: Array<number> = [];
    data = DATA;
    animated: boolean;

    constructor(public navCtrl: NavController, private zone: NgZone, private dbMeter: DBMeter) {
        this.dbMeter.start().subscribe(
            data => {
                this.zone.run(() => {
                if ( this.decibelArr.length < 100 ) {
                    this.decibelArr.push(data)
                } else {
                    this.decibelArr.shift()
                    this.decibelArr.push(data)
                }
                (data> 60) ? this.decibels = data : ''
                })
            }
        );
    }

    startCounter() {
        this.animated = true
        this.counterInterval = setInterval(() => {
            if(this.counter > 0) {
                this.counter --
            } else {
                clearInterval(this.counterInterval)
                this.animated = false
            }
            let entry = new Entry()
            entry.counter = moment().valueOf()
            this.data.push(entry)
        }, 2000)
    }

    stopCounter() {
        clearInterval(this.counterInterval)
        this.animated = false
    }

    resetCounter() {
        clearInterval(this.counterInterval)
        this.counter = 10
        this.data = []
    }

    tap() {
        let length = this.data.length
        let tapMoment = moment();
        let countMoment = this.data[length - 1].counter || false
        let diff = tapMoment.diff(countMoment, 'milliseconds')
        this.zone.run(() => {
        })
        let result= '';
        if(diff < 60) {
            result = 'good'
        } else if(diff < 120) {
            result = 'medium'
        } else {
            result = 'bad'
        }
        if(!this.data[length - 1].tap) {
            this.data[length - 1].tap = moment().valueOf();
            this.data[length - 1].diff = diff;
            this.data[length - 1].class = result
            console.log(this.data);
        }
    }
}