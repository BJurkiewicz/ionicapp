import { Component, NgZone } from '@angular/core';
import { NavController } from 'ionic-angular';

import { BatteryStatus } from '@ionic-native/battery-status';
import { Gyroscope, GyroscopeOrientation, GyroscopeOptions } from '@ionic-native/gyroscope';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { Shake } from '@ionic-native/shake';
import { DeviceOrientation, DeviceOrientationCompassHeading } from '@ionic-native/device-orientation';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  batterystatus: number = 0;
  batteryplugged: any;
  orientationX: any;
  orientationY: any;
  orientationZ: any;
  gyroscopeTimestamp: any;
  shakeThePhone: String = 'Shake!';
  heading: any;

  constructor(public navCtrl: NavController, private batteryStatus: BatteryStatus, private zone: NgZone, private gyroscope: Gyroscope, private localNotifications: LocalNotifications, private shake: Shake, private deviceOrientation: DeviceOrientation) {

  //   let subscription = this.batteryStatus.onChange().subscribe(
  //     (status) => {
  //       this.zone.run(() => {
  //         this.batterystatus = status.level;
  //         this.batteryplugged = status.isPlugged
  //       })
  //       console.log(status.level, status.isPlugged);
  //       // alert(status.level + ' ' + status.isPlugged)
  //     }
  //   );

  //   let options: GyroscopeOptions = {
  //     frequency: 1000
  //   };

  //   this.gyroscope.getCurrent(options)
  //   .then((orientation: GyroscopeOrientation) => {
  //     this.zone.run(() => {
  //       this.orientationX = orientation.x
  //       this.orientationY = orientation.y
  //       this.orientationZ = orientation.z
  //       this.gyroscopeTimestamp = orientation.timestamp
  //     })
  //     // console.log(orientation.x, orientation.y, orientation.z, orientation.timestamp);
  //   })
  //   .catch()

  //   this.gyroscope.watch()
  //   .subscribe((orientation: GyroscopeOrientation) => {
  //       this.zone.run(() => {
  //         this.orientationX = orientation.x
  //         this.orientationY = orientation.y
  //         this.orientationZ = orientation.z
  //         this.gyroscopeTimestamp = orientation.timestamp
  //       })
  //       // console.log(orientation.x, orientation.y, orientation.z, orientation.timestamp);
  //   });

  //   this.localNotifications.schedule({
  //     text: 'Delayed ILocalNotification',
  //     at: new Date(new Date().getTime() + 3600),
  //     led: 'FF0000',
  //     sound: null
  //   });

  //   const watch = this.shake.startWatch().subscribe(() => {
  //     this.zone.run(() => {
  //       this.shakeThePhone = 'Thanks!';
  //     })
  //   });

  //   this.deviceOrientation.getCurrentHeading().then(
  //     (data: DeviceOrientationCompassHeading) => this.heading = data.magneticHeading,
  //     (error: any) => console.log(error)
  //   );

  //   var subscribeDeviceOrientation = this.deviceOrientation.watchHeading().subscribe(
  //     (data: DeviceOrientationCompassHeading) => this.heading = data.magneticHeading
  //   );
  // }

  // showNotif() {

  //   this.localNotifications.schedule({
  //     title: 'Hello !',
  //     text: 'I am a notification with a badge',
  //     badge: 4,
  //     led: 'FF0000',
  //     sound: null,
  //   });
  // }
  // }
  }
}
